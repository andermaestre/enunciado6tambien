﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClasesSocket;

namespace ClienteForm
{
    public partial class Form1 : Form
    {
        Cliente c;
        public delegate void AnhadirLista(ListBox lb, String datos);
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            c = new Cliente(txtHost.Text, int.Parse(txtSocket.Text));
            c.Conectar();
            c.OnDatosRecibidos += C_OnDatosRecibidos;
        }

        private void C_OnDatosRecibidos(string datos)
        {
            AnhadirLista f = AnhadirFun;
            lb.BeginInvoke(f, new object[] { lb, datos });           
        }

        private void AnhadirFun(ListBox lb, string datos)
        {
            lb.Items.Add(datos);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            c.EnviarDatos(txtPalebria.Text);
        }
    }
}
