﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClasesSocket;

namespace Enunciado6
{
    public delegate void MostrarConexiones(TextBox tb,int val);
    public delegate void AnhadirNombre(ListBox lb,String nom);
    public partial class Form1 : Form
    {
        Servidor s;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            s = new Servidor(int.Parse(txtSocket.Text), "192.168.1.35", false);
            s.Start();

            s.OnDesconexionNoPermitida += () =>
            {
                MessageBox.Show("No se te permite la desconexion mortal", "Palabra de DeuxMachina");
            };
            s.OnNuevaConexion += (x,y) =>
            {
                MostrarConexiones f = CambiaConexiones;
                txtConexiones.BeginInvoke(f, new object[] { txtConexiones, s.ClientesConectados });
            };
            s.OnDatosRecibidos += S_OnDatosRecibidos;
        }

        private void S_OnDatosRecibidos(string datos, int pos)
        {
            AnhadirNombre f = NuevoNombre;
            lb.BeginInvoke(f, new object[] { lb, datos});
            string res = "Dice "+s.Procesos[pos].puerto.RemoteEndPoint.ToString()+": "+ datos;
            s.EnviarDatos(res);
        }

        private void NuevoNombre(ListBox lb, string nom)
        {
            lb.Items.Add(nom);
        }

        private void CambiaConexiones(TextBox tb, int val)
        {
            tb.Text = val.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            s.Cerrar();

        }
    }
}
